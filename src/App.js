import { Container, Row } from 'reactstrap';
import Banner from './components/Banner';
import Login from './components/Login';

import './App.css';

function App() {
  return (
    <Container fluid className="app">
      <Row className="app-row">
        <Banner md={4} xs={12} />
        <Login md={8} xs={12} />
      </Row>
    </Container>
  );
}

export default App;
