import {
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Label,
} from 'reactstrap';

import './Login.css';

export default function Login({ md, xs }) {
  return (
    <Col className="login-container" md={md} xs={xs}>
      <span className="login-link">
        Already have an account? <a href="/">Log in</a>
      </span>
      <div className="register-form">
        <h2>Register</h2>
        <p className="mb-5">Welcome! Please enter our account details.</p>
        <Form>
          <FormGroup className="mb-3">
            <Label htmlFor="email">Email Address</Label>
            <InputGroup>
              <Input className="input" type="email" id="email" />
              <InputGroupAddon addonType="append">
                <InputGroupText className="input-icon">
                  <i className="bi bi-person-lines-fill" />
                </InputGroupText>
              </InputGroupAddon>
            </InputGroup>
          </FormGroup>
          <FormGroup className="mb-3">
            <Label htmlFor="password">Password</Label>
            <InputGroup>
              <Input className="input" type="password" id="password" />
              <InputGroupAddon addonType="append">
                <InputGroupText className="input-icon">
                  <i className="bi bi-arrow-repeat" />
                </InputGroupText>
              </InputGroupAddon>
            </InputGroup>
          </FormGroup>
          <FormGroup className="mb-3">
            <Label htmlFor="password-retype">Retype Password</Label>
            <InputGroup>
              <Input className="input" type="password" id="password-retype" />
              <InputGroupAddon addonType="append">
                <InputGroupText className="input-icon">
                  <i className="bi bi-arrow-repeat" />
                </InputGroupText>
              </InputGroupAddon>
            </InputGroup>
          </FormGroup>
          <button type="submit" className="btn btn-primary">
            Register
          </button>
        </Form>
      </div>
    </Col>
  );
}
