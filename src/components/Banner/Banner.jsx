import { Button, Col } from 'reactstrap';
import './Banner.css';
import logo from '../../assets/logo.png';

export default function Banner ({ md, xs }) {
  return (
    <Col className="banner" md={md} xs={xs}>
      <img className="logo" src={logo} alt="logo"/>
      <div className="banner-text">
        <h1 className="mb-4">Video Platform we hope you'll love</h1>
        <p className="mb-4">Enjoy videos without ads. Engage with your favourite
          creators.
          Support awesome work.</p>
        <Button type="button" className="btn btn-outline-light">Go Home</Button>
      </div>
    </Col>
  );
}